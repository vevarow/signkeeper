import React from 'react';
import { NavLink } from 'react-router-dom';
// import { Redirect } from 'react-router';

import Content from './Content';

const Page = (WrappedComponent, allowedRoles, auth) =>
  class WithAuthorization extends React.Component {
    render() {
      const { role } = auth;
      if (typeof allowedRoles === 'undefined' || allowedRoles.includes(role)) {
        return (
          <Content size={WrappedComponent.contentSize}>
            <WrappedComponent auth={auth} />
          </Content>
        );
      }
      return (
        <div>
          <Content size="medium">
            <p>
             Для работы с сервисом <NavLink to="/signin">войдите</NavLink> или <NavLink to="/signup">зарегистрируйтесь</NavLink>
            </p>
          </Content>
          {/* <Redirect to="/signin" /> */}
        </div>
      );
    }
  };

export default Page;
