import React from 'react';
import { NotificationManager } from 'react-notifications';
import ReactTable from 'react-table';
import 'react-table/react-table.css';

import FileHash from './FileHash';
import { getData, postData } from '../helpers/fetcher';
import '../styles/Upload.css';


class Upload extends React.Component {
  static contentSize = 'large';

  static responseToTableData(documents) {
    return Object.keys(documents).map(value => ({
      id: documents[value].id,
      fileName: documents[value].name,
      hash: documents[value].hash,
      status: (documents[value].eth_block_hash !== null || documents[value].btc_block_hash !== null) ? 'Заверен' : 'Ожидает подтверждения',
      date: documents[value].date,
      txHashes: {
        eth: documents[value].eth_tx_hash,
        btc: documents[value].btc_tx_hash,
      },
      blockHashes: {
        eth: documents[value].eth_block_hash,
        btc: documents[value].btc_block_hash,
      },
      blockNumbers: {
        eth: documents[value].eth_block_number,
        btc: documents[value].btc_block_number,
      },
      gasUsed: documents[value].eth_gas_used,
      ethTxStatus: documents[value].eth_tx_status,
    }));
  }

  static renderRowInfo(rowInfo) {
    const info = [{
      title: 'Хэш документа',
      value: rowInfo.original.hash,
    }, {
      title: 'Дата загрузки',
      value: rowInfo.original.date,
    }, {
      title: 'Хэш транзакции в ETH',
      value: rowInfo.original.txHashes.eth,
    }, {
      title: 'Статус транзакции ETH',
      value: rowInfo.original.ethTxStatus,
    }, {
      title: 'Хэш блока ETH',
      value: rowInfo.original.blockHashes.eth,
    }, {
      title: 'Номер блока ETH',
      value: rowInfo.original.blockNumbers.eth,
    }, {
      title: 'Газ ETH',
      value: rowInfo.original.gasUsed,
    }, {
      title: 'Хэш транзакции в BTC',
      value: rowInfo.original.txHashes.btc,
    }, {
      title: 'Хэш блока BTC',
      value: rowInfo.original.blockHashes.btc,
    }, {
      title: 'Номер блока BTC',
      value: rowInfo.original.blockNumbers.btc,
    }];
    return (
      <table className="doc-info-table">
        <tbody>
          {info.map(value => (
            <tr key={value.title} className="doc-info-table--row">
              <td className="doc-info-table--row--title">{value.title}</td>
              <td className="doc-info-table--row--value">{value.value}</td>
            </tr>
        ))}
        </tbody>
      </table>
    );
  }

  constructor(props) {
    super(props);

    this.sendFileData = this.sendFileData.bind(this);
    this.addItemFirst = this.addItemFirst.bind(this);
    this.onTableParamsChange = this.onTableParamsChange.bind(this);

    this.state = {
      data: [],
      pages: -1,
      loading: true,
    };

    this.tableColumns = [{
      Header: 'Загруженные документы',
      columns: [{
        Header: 'ID',
        accessor: 'id',
      }, {
        Header: 'Название файла',
        accessor: 'fileName',
      }, {
        Header: 'Статус',
        accessor: 'status',
      }],
    }];
    // Fetch is being called by ReactTable
  }

  onTableParamsChange(state) {
    this.setState({ loading: true });
    getData('/documents', {
      page: state.page + 1,
      recordsShow: state.pageSize,
    })
      .then((res) => {
        if (res.status !== '200') {
          const error = 'Ошибка получения списка документов';
          NotificationManager.error(JSON.stringify(res), error);
          throw Error(error);
        } else {
          return res;
        }
      })
      .then((res) => {
        this.setState({
          data: Upload.responseToTableData(res.response),
          pages: parseInt(res.pagesCount, 10),
          loading: false,
        });
      });
  }

  sendFileData(fileInfo) {
    // TODO: Убрать костыль
    const curDate = new Date();
    /* eslint-disable prefer-template */
    const date = ((curDate.getFullYear()) + '-' + ((curDate.getMonth() + 1 < 10) ? '0' + (curDate.getMonth() + 1) : (curDate.getMonth() + 1))
      + '-' + ((curDate.getDay() < 10) ? '0' + curDate.getDay() : curDate.getDay()) + ' ' +
      ((curDate.getHours() < 10) ? '0' + curDate.getHours() : curDate.getHours()) + ':' +
      ((curDate.getMinutes() < 10) ? '0' + curDate.getMinutes() : curDate.getMinutes())
      + ':' + ((curDate.getSeconds() < 10) ? '0' + curDate.getSeconds() : curDate.getSeconds()));
    /* eslint-enable prefer-template */

    return postData('/upload', { ...fileInfo, date })
      .then((res) => {
        if (res.status !== '200') {
          NotificationManager.error(JSON.stringify(res), 'Ошибка добавления файла');
        } else {
          this.addItemFirst(fileInfo, date);
        }
      });
  }

  addItemFirst(fileInfo, fileDate) {
    this.setState({
      data: [{
        fileName: fileInfo.name,
        hash: fileInfo.hash,
        status: 'Ожидает подтверждения',
        date: fileDate,
        txHashes: {
          eth: null,
          btc: null,
        },
        blockHashes: {
          eth: null,
          btc: null,
        },
        blockNumbers: {
          eth: null,
          btc: null,
        },
        gasUsed: null,
        ethTxStatus: null,
      }, ...this.state.data],
    });
  }

  render() {
    const { data } = this.state;
    return (
      <div>
        <FileHash callback={this.sendFileData} />
        <ReactTable
          manual
          pages={this.state.pages}
          loading={this.state.loading}
          data={data}
          columns={this.tableColumns}
          defaultPageSize={10}
          className="-striped -highlight"
          SubComponent={Upload.renderRowInfo}
          onFetchData={this.onTableParamsChange}
        />
        <br />
      </div>
    );
  }
}

export default Upload;
