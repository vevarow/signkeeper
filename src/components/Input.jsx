import React, { Component } from 'react';

class Input extends Component {
  render() {
    return (
        <input
          className="form-control"
          onChange={this.props.func}
          name={this.props.name}
          type={this.props.type}
          value={this.props.value}
          placeholder={this.props.text}
        />
    );
  }
}

export default Input;
