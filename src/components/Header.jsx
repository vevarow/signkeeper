import React from 'react';
import { NavLink } from 'react-router-dom';
import {
  Navbar,
  NavbarBrand
} from 'reactstrap';
import PropTypes from 'prop-types';

import Menu from './Menu';
import Logo from './Logo';

import routes from '../routes';

import '../styles/Header.css';

class Header extends React.Component {
  static defaultProps = {
    login: '',
  };

  static propTypes = {
    loggedIn: PropTypes.bool.isRequired,
    login: PropTypes.string,
  };

  render() {
    const { loggedIn } = this.props;
    return (
      <Navbar light expand="md">
        <NavbarBrand className="logo-link-container" href="/">
          <Logo />
        </NavbarBrand>
        <div className="spaceholder" />
        <Menu
          list={routes.filter(value => typeof value.title !== 'undefined')
            .filter(value =>
              (!value.displayForGuestsOnly && loggedIn) ||
              (value.displayForGuestsOnly && !loggedIn))
          }
        />
        {this.props.login && this.props.login.length !== 0 &&
        <div className="user-login">
          {this.props.login}
          <NavLink className="user-login-logout" to="/logout">
            Выйти
          </NavLink>
        </div>
        }
      </Navbar>
    );
  }
}

export default Header;
