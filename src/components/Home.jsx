import React from 'react';
import { NavLink } from 'react-router-dom';
import { Button } from 'reactstrap';

import Description from './Description';
import '../styles/Home.css';

class Home extends React.Component {
  render() {
    return (
      <div className="home">
        <Description />
        <NavLink to="/upload">
          <Button className="home-check-button">
            <p className="home-check-button-text">Проверить документ</p>
          </Button>
        </NavLink>
      </div>
    );
  }
}

export default Home;
