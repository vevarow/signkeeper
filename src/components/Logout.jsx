import React from 'react';
import { Redirect } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';

import authType from '../types/auth';

class Logout extends React.Component {
  static propTypes = {
    auth: authType.isRequired,
  };

  render() {
    this.props.auth.logout()
      .then(() => {
        NotificationManager.success('Вы успешно вышли из системы');
      });

    return <Redirect to="/" />;
  }
}

export default Logout;
