import React from 'react';

class Description extends React.Component {
  render() {
    return (
      <div>
        <p className="description-text">Lorem ipsum dolor sit amet, <br />
          consectetuer adipiscing elit, sed diam nonummy nibh
        </p>
      </div>
    );
  }
}

export default Description;
