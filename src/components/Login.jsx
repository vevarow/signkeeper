import React from 'react';
import { Redirect } from 'react-router';

import { Button, Alert, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvFeedback } from 'availity-reactstrap-validation';

import authType from '../types/auth';
import { reduceErrorMessage } from '../helpers/fetcher';

import '../styles/Login.css';

class Login extends React.Component {
  static contentSize = 'small';

  static propTypes = {
    auth: authType.isRequired,
  };

  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);

    this.state = {
      error: null,
    };
  }

  handleInputChange(event) {
    this.setState({
      error: null,
      formData: {
        ...this.state.formData,
        [event.target.name]: {
          valid: event.target.checkValidity(),
          value: event.target.value,
        },
      },
    });
  }

  handleSubmit(event, errors, values) {
    if (Object.keys(errors).length !== 0) {
      // TODO: Избавиться от eslint-disable
      // eslint-disable-next-line react/no-unused-state
      this.setState({ errors, values });
      return;
    }

    // TODO: Refactor error handling
    this.props.auth.login(values.email, values.password)
      .then((res) => {
        if (res.status === '400') {
          this.setState({
            error: reduceErrorMessage(res.response),
          });
        }
      });
  }

  renderForm() {
    return (
      <AvForm onSubmit={this.handleSubmit}>
        { this.state.error &&
          <Alert color="danger">{ this.state.error }</Alert>
        }
        <AvGroup>
          <Label for="email" className="input-label">E-Mail</Label>
          <AvInput
            className="input-container"
            type="email"
            name="email"
            id="email"
            placeholder=""
            onChange={this.handleInputChange}
            required
          />
          <AvFeedback>
            Неправильный e-mail
          </AvFeedback>
        </AvGroup>
        <AvGroup>
          <Label for="password" className="input-label">Пароль</Label>
          <AvInput
            // https://stackoverflow.com/a/21456918
            pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,16}$"
            className="input-container"
            type="password"
            name="password"
            id="password"
            placeholder=""
            onChange={this.handleInputChange}
            required
          />
          <AvFeedback>
            Пароль должен быть от 8 до 20 символов,
            состоять из латинских букв и хотя бы одной цифры
          </AvFeedback>
        </AvGroup>
        <div className="submit-container">
          <Button className="login-form-submit">Войти</Button>
        </div>
      </AvForm>
    );
  }

  render() {
    if (this.props.auth.loggedIn) {
      return <Redirect to="/upload" />;
    }

    return (
      <div>
        {this.renderForm()}
      </div>
    );
  }
}

export default Login;
