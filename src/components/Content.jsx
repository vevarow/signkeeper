import React from 'react';
import PropTypes from 'prop-types';

class Content extends React.Component {
  static defaultProps = {
    children: '',
    size: 'medium',
  };
  static propTypes = {
    children: PropTypes.node,
    size: PropTypes.oneOf(['small', 'medium', 'large']),
  };
  render() {
    return (
      <div className={`content content-${this.props.size}`}>
        {this.props.children}
      </div>
    );
  }
}

export default Content;
