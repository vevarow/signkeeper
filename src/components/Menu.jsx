import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';

import {
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
} from 'reactstrap';

import '../styles/Menu.css';

class Menu extends React.Component {
  static propTypes = {
    list: PropTypes.arrayOf(PropTypes.shape({
      title: PropTypes.string.isRequired,
      to: PropTypes.string.isRequired,
    })).isRequired,
  };
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
    };
  }
  toggle() {
    this.setState({
      isOpen: !this.state.isOpen,
    });
  }
  renderList() {
    return this.props.list.map(value => (
      <NavItem key={value.to}>
        <NavLink to={value.to} activeClassName="selected">{value.title}</NavLink>
      </NavItem>
    ));
  }
  render() {
    return (
      <React.Fragment>
        <NavbarToggler onClick={this.toggle} />
        <Collapse className="main-menu" isOpen={this.state.isOpen} navbar>
          <Nav className="ml-auto" navbar>
            {this.renderList()}
          </Nav>
        </Collapse>
      </React.Fragment>

    );
  }
}

export default Menu;
