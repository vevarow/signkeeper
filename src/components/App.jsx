import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import 'react-notifications/lib/notifications.css';

import Page from './Page';
import Header from './Header';

import { getData, postData, reduceErrorMessage } from '../helpers/fetcher';
import routes from '../routes';

import '../styles/App.css';

class App extends React.Component {
  static fetchUserInfo() {
    return getData('/me');
  }

  constructor(props) {
    super(props);

    this.renderRoutes = this.renderRoutes.bind(this);
    this.register = this.register.bind(this);
    this.login = this.login.bind(this);
    this.logout = this.logout.bind(this);

    const email = localStorage.getItem('email');
    this.state = {
      user: {
        loggedIn: typeof email === 'string',
        role: email ? 'user' : null,
        email,
      },
    };

    this.setUserInfo({ email });

    this.updateUserInfo();
  }

  setUserInfo(userInfo) {
    if (typeof userInfo !== 'undefined') {
      this.setState({
        user: {
          email: userInfo.email,
          role: 'user',
          loggedIn: true,
        },
      });
      localStorage.setItem('email', userInfo.email);
    } else {
      this.setState({
        user: {
          loggedIn: false,
        },
      });
      localStorage.removeItem('email');
    }
  }

  updateUserInfo() {
    App.fetchUserInfo()
      .then((res) => {
        // TODO: get rid of && res.response !== null
        if (res.status === '200' && res.response !== null) {
          this.setUserInfo({
            email: res.response.email,
          });
        } else {
          this.setUserInfo();
        }
      });
  }

  register({ name, email, password }) {
    return postData('/register', {
      name,
      email,
      password,
    })
      .then((res) => {
        if (res.status === '200') {
          this.updateUserInfo(res.response);
          NotificationManager.success('Вы успешно зарегистрированы');
        } else {
          NotificationManager.error(reduceErrorMessage(res.response));
        }
        return res;
      });
  }

  login(login, password) {
    return postData('/login', {
      email: login,
      password,
    })
      .then((res) => {
        if (res.status === '200') {
          this.setUserInfo({
            email: res.response,
          });
          NotificationManager.success('Вы успешно вошли в систему');
        } else {
          NotificationManager.error(reduceErrorMessage(res.response));
        }
        return res;
      });
  }

  logout() {
    this.setUserInfo();
    return getData('/logout');
  }

  renderRoutes(values) {
    return values.map(value => (
      <Route
        key={value}
        path={value.to}
        exact={value.exact}
        component={Page(value.component, value.roles, {
              loggedIn: this.state.user.loggedIn,
              role: this.state.user.role,
              register: this.register,
              login: this.login,
              logout: this.logout,
        })}
      />
    ));
  }

  render() {
    return (
      <div className="App">
        <Header loggedIn={this.state.user.loggedIn} login={this.state.user.email} />

        <Switch>
          {this.renderRoutes(routes)}
        </Switch>

        <NotificationContainer />
      </div>
    );
  }
}

export default App;
