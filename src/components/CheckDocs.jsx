import React, { Component } from 'react';

import Input from './Input';
import { postData } from '../helpers/fetcher';
import { SHA256 } from '../helpers/hash';

import '../styles/CheckDocs.css';

class CheckDocs extends Component {
  constructor() {
    super();
    this.state = {
      docHash: '',
      blockHash: '',
      waitingRes: false,
      successfulRes: false,
      msg: '',
    };
  }

  inputBlockHash = event => this.setState({ [event.target.name]: event.target.value });

  sendDoc = (event) => {
    event.preventDefault();
    if (!this.state.docHash) {
      this.setState({
        msg: 'Файл ещё не загружен в форму',
      });
      return;
    }

    this.setState({
      msg: 'Файл отправлен на проверку, ожидайте...',
      waitingRes: true,
    });
    postData('/check', {
      hash: this.state.docHash,
      blockHash: this.state.blockHash,
    })
      .then((answer) => {
        if (answer.status === '400') {
          this.setState({
            msg: `Что-то пошло не так: "${
              answer ? answer.response : 'ОШИБКА!!!'}"`,
            waitingRes: false,
            successfulRes: false,
          });
        } else {
          this.setState({
            msg: `Ответ получен: "${
              answer ? answer.response : 'ОШИБКА!!!!'}"`,
            waitingRes: false,
            successfulRes: true,
          });
        }
      })
      .catch((error) => {
        this.setState({
          msg: `Произошла ОШИБКА!!! "${
            error ? error.response : ' Нет ответа'}"`,
          waitingRes: false,
          successfulRes: false,
        });
      });
  };

  countHash = (event) => {
    if (event.target.files.length !== 0) {
      const FR = new FileReader();
      FR.readAsBinaryString(event.target.files[0]);
      FR.onload = (fileEvent) => {
        const countedHash = SHA256(fileEvent.target.result);
        this.setState({
          docHash: countedHash,
        });
      };
    }
  };

  render() {
    return (
      <form onSubmit={this.sendDoc}>
        {!this.state.successfulRes && this.state.msg && !this.state.waitingRes && <div className="alert alert-danger">{this.state.msg}</div>}
        {this.state.waitingRes && this.state.msg && <div className="alert alert-info">{this.state.msg}</div>}
        {this.state.successfulRes && this.state.msg && <div className="alert alert-success">{this.state.msg}</div>}
        <input type="file" name="loadFile" onChange={this.countHash} />
        <Input text="Полученный хеш" type="text" name="docHash" value={this.state.docHash} />
        <Input text="Введите хэш блока (необязательно) " type="text" name="blockHash" value={this.state.blockHash} func={this.inputBlockHash} />
        <button className="btn btn-primary" onClick={this.sendDoc}>Отправить</button>
      </form>
    );
  }
}

export default CheckDocs;
