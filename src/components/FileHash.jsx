import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, FormGroup, FormText, Input } from 'reactstrap';

import { SHA256 } from '../helpers/hash';

class FileHash extends Component {
  static propTypes = {
    callback: PropTypes.func,
  };

  static defaultProps = {
    callback: Promise.resolve(true),
  };

  static countHash(file) {
    return new Promise((resolve) => {
      const reader = new FileReader();
      reader.readAsBinaryString(file);
      reader.onload = (fileEvent) => {
        const docHash = SHA256(fileEvent.target.result);
        resolve(docHash);
      };
    });
  }

  constructor(props) {
    super(props);

    this.handleOnChange = this.handleOnChange.bind(this);

    this.state = {
      disabled: false,
    };
  }

  handleOnChange(event) {
    const file = event.target.files[0];

    this.setState({
      disabled: true,
    });

    FileHash.countHash(file)
      .then((hash) => {
        const { name } = file;
        const { callback } = this.props;

        this.setState({ hash });

        callback({ name, hash })
          .then(() => {
            this.setState({
              disabled: false,
            });
          });
      });
  }

  render() {
    return (
      <Form>
        <FormGroup>
          <Input disabled={this.state.disabled} type="file" name="file" id="file" onChange={this.handleOnChange} />
        </FormGroup>
        {this.state.hash &&
        <FormGroup>
          <FormText>
            sha256: <span>{this.state.hash}</span>
          </FormText>
        </FormGroup>
        }
      </Form>
    );
  }
}

export default FileHash;
