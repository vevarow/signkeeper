import React from 'react';

import logo from '../assets/reliable-logo.svg';

import '../styles/Logo.css';

class Logo extends React.Component {
  render() {
    return (
      <div className="logo">
        <div className="logo-text">
          <img className="logo-img" alt="signkeeper" src={logo} />SIGNKEEPER
        </div>
      </div>
    );
  }
}

export default Logo;
