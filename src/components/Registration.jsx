import React, { Component } from 'react';
import { Redirect } from 'react-router';
import Input from './Input';

import authType from '../types/auth';

import '../styles/Registration.css';

class Registration extends Component {
  static contentSize = 'small';

  static propTypes = {
    auth: authType.isRequired,
  };

  constructor(props) {
    super(props);
    this.state =
        {
          userName: '',
          email: '',
          pass: '',
          passRep: '',
          success: false,
          msg: '',
        };
  }

    input = event => this.setState({ [event.target.name]: event.target.value });

    check = () => {
      if ((!this.state.email) || (!this.state.userName) ||
        (!this.state.pass) || (!this.state.passRep)) {
        this.setState({
          msg: 'Некоторые поля не заполнены',
        });
        return false;
      }

      if (!this.state.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
        this.setState({
          msg: 'Неверный формат почты. ' +
          'Email должен быть в таком формате - name@email.com',
        });
        return false;
      }

      if (this.state.pass !== this.state.passRep) {
        this.setState({
          msg: 'Пароли не совпадают',
        });
        return false;
      }

      if (!this.state.pass.match(/^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,16}$/)) {
        this.setState({
          msg: 'Пароль должен быть от 8 до 16 символов,' +
          ' состоять из латинских букв и хотя бы одной цифры.',
        });
        return false;
      }

      this.setState({
        msg: '',
      });
      return true;
    };

    submit = (event) => {
      event.preventDefault();
      if (this.check()) {
        this.props.auth.register({
          name: this.state.userName,
          email: this.state.email,
          password: this.state.pass,
        })
          .then((answer) => {
            if (answer.status === '200') {
              this.setState({
                success: true,
                msg: '',
              });
            } else {
              this.setState({
                msg: `${answer.response}`,
              });
            }
          });
      }
    };

    render() {
      return (
        <div>
          {this.state.success ?
            <Redirect to="/upload" />
            :
            <form onSubmit={this.submit}>
              {this.state.msg && <div className="alert alert-danger">{this.state.msg}</div>}
              <Input text="Имя пользователя" type="text" name="userName" func={this.input} value={this.state.userName} />
              <Input text="Email" type="email" name="email" func={this.input} value={this.state.email} />
              <Input text="Пароль" type="password" name="pass" func={this.input} value={this.state.pass} />
              <Input text="Повторить пароль" type="password" name="passRep" func={this.input} value={this.state.passRep} />
              <button type="submit" className="btn btn-primary" onClick={this.submit}>Подтвердить</button>
            </form>
          }
        </div>
      );
    }
}

export default Registration;
