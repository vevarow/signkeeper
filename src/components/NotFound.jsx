import React from 'react';
import { NavLink } from 'react-router-dom';

class NotFound extends React.Component {
  render() {
    return (
      <div>
        <h1>Cтраница не найдена</h1>
        <p>Проверьте корректность введенного адреса или перейдите на
          <NavLink to="/"> главную</NavLink>
        </p>
      </div>
    );
  }
}

export default NotFound;
