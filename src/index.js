import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Root from './components/Root';
import registerServiceWorker from './registerServiceWorker';

// в качестве точки входа create-react-app ищет файл с названием index.js
// eslint-disable-next-line react/jsx-filename-extension
ReactDOM.render(<Root />, document.getElementById('root'));
registerServiceWorker();
