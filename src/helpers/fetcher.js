import { NotificationManager } from 'react-notifications';

const apiHost = '';

const defaultParams = {
  credentials: 'same-origin', // include, same-origin, *omit
  redirect: 'follow', // manual, *follow, error
  referrer: 'no-referrer', // *client, no-referrer
  mode: 'cors',
  headers: {
    'content-type': 'application/json',
    // 'Access-Control-Allow-Origin': apiHost.slice(2),
  },
};

const reduceErrorObject = (errorObj) => {
  const errorMessage = Object.keys(errorObj)
    .map(key => errorObj[key])
    .reduce((accumulator, value) => `${accumulator}\n${value.join('\n')}`, '');

  return errorMessage.length ? errorMessage : 'Неизвестная ошибка';
};

const handleFetchErrors = (res) => {
  if (!res.ok) {
    throw Error('Не удалось выполнить запрос. Проверьте интернет-соединение.');
  }
  return res.json();
};

const handleFetchCatch = (err) => {
  NotificationManager.error(err.message, 'Ошибка запроса');
  // eslint-disable-next-line no-console
  console.error('Fetch error', err);
};

const objectToQueryParam = obj =>
  Object.keys(obj).map(value => `${value}=${obj[value]}`).join('&');

export const reduceErrorMessage = (error) => {
  // the most reliable way to determine variable instance type
  // https://stackoverflow.com/questions/12996871/why-does-typeof-array-with-objects-return-object-and-not-array
  switch (Object.prototype.toString.call(error).split(' ')[1].slice(0, -1)) {
    case 'Array':
      return error.join('\n');
    case 'Object':
      return reduceErrorObject(error);
    default:
      return error;
  }
};

const createFetcher = (url, params) =>
  fetch(`${apiHost}${url}`, { ...defaultParams, ...params })
    .then(handleFetchErrors)
    .catch(handleFetchCatch);

export const getData = (url, optParams) =>
  createFetcher(`${url}${typeof optParams !== 'undefined' ?
    `?${objectToQueryParam(optParams)}` :
    ''}`, defaultParams);

export const postData = (url, data) => {
  const body = JSON.stringify(data);
  const params = {
    body,
    method: 'POST',
  };
  return createFetcher(url, params);
};
