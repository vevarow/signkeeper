// https://github.com/emn178/js-sha256
export const SHA256 = (text) => {
  const sha256 = require('js-sha256');
  const hash = sha256(text);
  return hash;
};