import Home from './components/Home';
import Registration from './components/Registration';
import Login from './components/Login';
import Logout from './components/Logout';
import Upload from './components/Upload';
import CheckDocs from './components/CheckDocs';
import AdminPage from './components/AdminPage';
import NotFound from './components/NotFound';


export default [{
  to: '/',
  exact: true,
  component: Home,
}, {
  to: '/admin',
  component: AdminPage,
  roles: ['user'],
}, {
  title: 'Проверить',
  to: '/check',
  component: CheckDocs,
  roles: ['user'],
}, {
  title: 'Добавить',
  to: '/upload',
  component: Upload,
  roles: ['user'],
}, {
  title: 'Войти',
  to: '/signin',
  // TODO: Костыль
  displayForGuestsOnly: true,
  component: Login,
}, {
  title: 'Регистрация',
  to: '/signup',
  // TODO: Костыль
  displayForGuestsOnly: true,
  component: Registration,
}, {
  to: '/logout',
  component: Logout,
  roles: ['user'],
}, {
  component: NotFound,
}];
