import PropTypes from 'prop-types';

export default PropTypes.shape({
  register: PropTypes.func,
  login: PropTypes.func,
  logout: PropTypes.func,
  loggedIn: PropTypes.bool,
});
